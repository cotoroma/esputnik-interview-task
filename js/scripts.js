'use strict';

class Menu {
  #activeColor = "#E0E0E0";
  #notActiveColor = "#FFF";
  #activeMenu = undefined;
  #activeSubMenu = undefined;

  constructor(el) {
    this.el = el;
    this.init();
  }

  init() {
    document.querySelectorAll('.submenu-item').forEach(item => {
      this.addEvent(item, 'mousedown', e => {
        this.clearActive();
        this.activeSubMenu = e.target; /// li
        this.activeMenu = e.target.parentElement.parentElement.querySelector('.menu-label'); /// li -> ul -> li -> span
        this.setActive();
        this.setLocation(this.activeSubMenu.dataset.href);
      });
    });
  }

  addEvent(el, type, handler) {
    if (el.addEventListener){
      el.addEventListener(type, handler, false)
    } else {
      el.attachEvent('on' + type, handler)
    }
  }

  setLocation(curLoc){
    try {
      history.pushState(null, null, curLoc);
      return;
    } catch(e) {}

    location.hash = '#' + curLoc;
  }

  clearActive() {
    if (this.activeSubMenu)
      this.activeSubMenu.style.backgroundColor = this.#notActiveColor;
    if (this.activeMenu)
      this.activeMenu.style.backgroundColor = this.#notActiveColor;
  }
  setActive() {
    this.activeSubMenu.style.backgroundColor = this.#activeColor;
    this.activeMenu.style.backgroundColor = this.#activeColor;
  }
}

window.addEventListener('load', function() {
  const menu = new Menu(
    document.querySelector('#menu')
  );
});